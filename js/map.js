var map, geocoder, service;

// Initialise la carte
function initMap(){
	// Création de la carte avec la configuration par défaut
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: defaultZoom,
		center: defaultPosition,
    disableDoubleClickZoom: true
	});
	// Création d'objets Gmap -> Geocoder et Place
	geocoder = new google.maps.Geocoder();
  service = new google.maps.places.PlacesService(map);

	// Tente de localiser le visiteur
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
				$pos = new google.maps.LatLng(parseFloat(position.coords.latitude),parseFloat(position.coords.longitude));
        map.setCenter($pos);

				var icon = {
			    path: "M32 19l-6-6v-9h-4v5l-6-6-16 16v1h4v10h10v-6h4v6h10v-10h4z",
			    fillColor: color,
			    fillOpacity: 1,
			    anchor: new google.maps.Point(0,0),
			    strokeWeight: 0,
			    scale: 1.5
			  }

        $marker = new google.maps.Marker({
          position: $pos,
          map: map,
          title: 'Vous êtes ici',
          icon: icon
        });
			},function() {
      	handleLocationError(true, map.getCenter());
			});
		} else {
			handleLocationError(false, map.getCenter());
    }

    function handleLocationError(browserHasGeolocation, $pos) {
			var infoWindow = new google.maps.InfoWindow({map: map});
			infoWindow.setPosition($pos);
			infoWindow.setContent(browserHasGeolocation ?
				'Error: The Geolocation service failed.' :
				'Error: Your browser doesn\'t support geolocation.');
    }

		// Map listener : double click -> formulaire pour ajuoter un restaurant
		map.addListener('dblclick', function(event){
			// Ouverture du modal
			$('#formRestaurant').unbind('submit').submit();
			$('#newRestaurantName').val('');
			$('#newRestaurantAddress').val('');
			var clickPosition = event.latLng;
			$('#placeRestaurant').modal('open');
			$('#newRestaurantName').focus();
			geocoder.geocode({'location': clickPosition}, function(results, status) {
				if (status === 'OK') {
					if (results[0]) {
						$('#newRestaurantAddress').val(results[0].formatted_address);
					} else {
						window.alert('No results found');
					}
				} else {
					window.alert('Geocoder failed due to: ' + status);
				}
			});
			// Envoi du modal
			$('#formRestaurant').submit(function(){
				$('#placeRestaurant').modal('close');
				var restaurantName = $('#newRestaurantName').val();
				var address = $('#newRestaurantAddress').val();
				var index = allRestaurants.length;
				var nbMarker = (index+1).toString();
				newRestaurant(restaurantName, address, clickPosition.lat(), clickPosition.lng(), new Array());
				newMarker(clickPosition, nbMarker, this.restaurantName, index);
			});
		});

		// Map listener : au déplacement de la carte -> lance onMapMove
		map.addListener('bounds_changed', function(){
			onMapMove();
		});

		// Map listener : drag end -> cherche dans les alentours les restaurants de Google
		map.addListener('dragend', function(){
      if (map.getZoom() >= minZoomAutoComplete) {
    		position = map.getCenter();
    		addCloseRestaurant(position);
    	}
    });
}

// Cherche les restaurants proches du centre de la carte
function addCloseRestaurant(position){
	var request = { location: position, radius:'1000', types: ['restaurant'] };

	function callback(results, status){
		if(status == google.maps.places.PlacesServiceStatus.OK){
			$.each(results,function(){
				$placeID = {placeId: this.place_id};
				service.getDetails($placeID, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK){
						$position = results.geometry.location;
						$doesItExist = false;
						$.each(allMarkers, function(index){
							if(this.getPosition().equals($position)){
								$doesItExist = true;
							}
							if(((index+1) === allMarkers.length) && ($doesItExist === false)){
								addRestaurantWithSearch($position, results)
							}
						});
					}
				});
			});
		}
	};
	service.nearbySearch(request, callback);
}

// Ajoute les restaurants trouvés sur Google Place
function addRestaurantWithSearch(position, results){

	$restaurantName = results.name;
	$address = (results.formatted_address).slice(0, -8);
	$lat = results.geometry.location.lat();
	$lng = results.geometry.location.lng();
	$index = allRestaurants.length;
	$nbMarker = ($index+1).toString();
	$ratingsTab = new Array();

	if ($.type(results.reviews) === "array"){
			$.each(results.reviews, function(){
					$stars = Number(this.rating);
					$comment = this.text;
					$aRating = new Rating($stars, $comment);
				  $ratingsTab.push($aRating);
			});
	}
	newRestaurant($restaurantName, $address, $lat, $lng, $ratingsTab);
	newMarker(position, $nbMarker, this.restaurantName, $index);
}

// Vérifie si chaque marqueur est dans la zone et compris dans la filtre
function onMapMove(){
  $.each(allMarkers, function (index, marker){
		var restaurantElt = $('.restaurant:eq(' + index + ')');
		if(map.getBounds().contains(marker.getPosition()) && allRestaurants[index].isInRange() == true){
			marker.setVisible(true);
			restaurantElt.removeClass('hide');
		} else {
			marker.setVisible(false);
			restaurantElt.addClass('hide');
		}
		if(map.getBounds().contains(marker.getPosition()) && marker.getVisible() && marker.getVisible() && allRestaurants[index].isInRange() == true){
			restaurantElt.removeClass('hide');
		} else {
			restaurantElt.addClass('hide');
		}
	});
}
