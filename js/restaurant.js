function Restaurant(name, address, lat, lng, ratings){
  this.name = name;
  this.address = address;
  this.lat = lat;
  this.lng = lng;
  this.ratings = ratings;
}

// Retourne la moyenne de ce restaurant
Restaurant.prototype.getAverage = function(){
  if (this.ratings.length == 0) {
    return 0;
  } else {
    $sum = 0;
    $.each(this.ratings, function(index){
      $sum = $sum + this.stars;
    });
    $average = $sum / this.ratings.length;
    return $average;
  }
}

// Met à jour la moyenne de ce restaurant
Restaurant.prototype.updateAverageDiv = function(index){
  $('.average:eq(' + index + ')').rateYo("rating", this.getAverage());
}

// Ajoute une note à ce restaurant
Restaurant.prototype.addRating = function(stars, comment){
  $aRating = new Rating(stars, comment);
  this.ratings.push($aRating);
  onMapMove();
}

// Vérifie si ce restaurant est compris dans le filtre
Restaurant.prototype.isInRange = function(){
  $currentAverage = this.getAverage();
  return Number($currentAverage) >= minRate && Number($currentAverage) <= maxRate ? true : false;
}

// Ajoute ce restaurant au DOM
Restaurant.prototype.buildRestaurantDiv = function(index){
  $restaurantListElt = $('#restaurantList');
  $li = $('<li></li>').addClass('restaurant').attr('tabindex', -1).appendTo($restaurantListElt);

  // RESTAURANT
  $thisRestaurant = $('<div></div>').addClass('collapsible-header').appendTo($li);
  $restaurantBloc = $('<div></div>').addClass('col-xs-12').appendTo($thisRestaurant);

  $restaurantLocation = "location=" + this.lat + "," + this.lng;
  $streetViewImg = $('<img>').addClass('streetview')
  .attr('src', '//maps.googleapis.com/maps/api/streetview?size=100x100&key=AIzaSyCY2YSw8RU-njyTNebCy0-FlH33LleOt-4&' + $restaurantLocation)
  .appendTo($restaurantBloc);

  $localize = $('<img>').addClass('localize').attr('src', './img/loc.svg').appendTo($restaurantBloc);
  $localize.click({index: index}, goToRestaurant);

  if (this.ratings != '') {
    $('<div></div>').addClass('average')
    .rateYo({ rating: this.getAverage(), halfStar: true, starWidth: "20px", ratedFill: color, readOnly: true })
    .appendTo($restaurantBloc);
  } else {
    $('<div></div>').addClass('average')
    .rateYo({ rating: 0, halfStar: true, starWidth: "20px", ratedFill: color, readOnly: true })
    .appendTo($restaurantBloc);
  }

  $('<span></span>').addClass('id').text(index+1).appendTo($restaurantBloc);
  $('<span></span>').addClass('name').text(this.name).appendTo($restaurantBloc);
  $('<div></div>').addClass('address').text(this.address).appendTo($restaurantBloc);


  $liBody = $('<div></div>').addClass('collapsible-body').appendTo($li);

  // RATINGS
  $('<div></div>').addClass('ratings').appendTo($liBody);
  for (var i = 0; i < this.ratings.length; i++){ this.buildRatingDiv(index, i); }

  // FORM
  $newCommentForm = $('<div></div>').addClass('commentForm').appendTo($liBody);
  $sep = $('<hr>').appendTo($newCommentForm);
  $title = $('<div></div>').addClass('newCommentLabel').html('Votre avis sur ce restaurant : ').appendTo($newCommentForm);
  $form = $("<form></form>").addClass('newRatingForm').appendTo($newCommentForm);
  $newRate = $('<div></div>').addClass('newStars')
  .rateYo({ rating: 2.5, halfStar: true, starWidth: "20px", ratedFill: color });
  $form.append($newRate);
  $form.append('<textarea class="textArea newComment" placeholder="Votre commentaire ici"></textarea>');
  $form.append('<input type="button" class="btn" value="Envoyer" onclick="newRating(' + index + ')">');
}

// Ajoute les notes de ce restaurant au DOM
Restaurant.prototype.buildRatingDiv = function(restaurant, index){
  $hisRatings = $('.ratings:eq(' + restaurant + ')');
  $('<span></span>')
  .rateYo({ rating: this.ratings[index].stars, halfStar: true, starWidth: "20px", ratedFill: color, readOnly: true })
  .appendTo($hisRatings);
  $('<div></div>').addClass('rate').text(this.ratings[index].comment).appendTo($hisRatings);
}
