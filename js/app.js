// Quand le document est prêt
$(document).ready(function(){
  // Active le plugIn collapse
  $('.collapsible').collapsible();
  // Initialise et montre les filtres
  initFilter();
});

window.onload=function() {
	initColor();
};

// Récupère les données du fichier JSON
$.getJSON('./data/restaurants.json', function(data){
  $.each(data, function(index){
    $label = (index+1).toString();
    $markerPosition = { lat: this.lat, lng: this.lng };
    // Créé et affiche un nouveau restaurant
    newRestaurant(this.restaurantName, this.address, this.lat, this.lng, this.ratings);
    // Créé et affiche un nouveau marqueur
    newMarker($markerPosition, $label, this.restaurantName, index);
  });
  // Active le modal de materialize pour l'ajout de restaurants
  $('#placeRestaurant').modal();
});

// Ajoute une nouvelle note
function newRating(index){
  $comment = $('.newComment:eq(' + index + ')').val();
  if ($comment != '') {
    $stars = $('.newStars:eq(' + index + ')').rateYo("rating");
    allRestaurants[index].addRating($stars, $comment);
    allRestaurants[index].buildRatingDiv(index, (allRestaurants[index].ratings.length - 1));
    allRestaurants[index].updateAverageDiv(index);
    $('.newComment:eq(' + index + ')').val('');
  }
}

// Ajoute un nouveau restaurant
function newRestaurant(name, address, lat, lng, ratings){
  $index = allRestaurants.length;
  $aRestaurant = new Restaurant(name, address, lat, lng, ratings);
  allRestaurants.push($aRestaurant);
  allRestaurants[$index].buildRestaurantDiv($index);
}

// Ajoute un nouveau marqueur
function newMarker(position, label, title, index){

  var icon = {
    path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
    fillColor: color,
    fillOpacity: .9,
    anchor: new google.maps.Point(0,0),
    strokeWeight: 0,
    scale: 1
  }

  $marker = new google.maps.Marker({
      position: position,
      label: {text: label, color: colorText},
      title: title,
      map: map,
      icon: icon
  });
  allMarkers.push($marker);
  $marker.addListener('click', function(){
    $('.collapsible').collapsible('open', index);
    $('.restaurant')[index].focus();
  });
}

// Affiche les filtres
function initFilter(){
  $filterElt = $('#filter');

  $minFilter = $('<span></span>').attr('id', 'minFilter')
  .rateYo({rating: 0, halfStar: true, starWidth: "20px", ratedFill: color,
    onSet: function (rating, rateYoInstance) {
      if (maxRate >= Number(rating)){
        minRate = rating;
        onMapMove();
      } else {
        maxRate = rating;
        minRate = rating;
        $('#maxFilter').rateYo("rating", minRate);
      }
    }
  })
  .appendTo($filterElt);
  $('<div></div>').addClass('filterLabel').text(noteMinimumText).appendTo($minFilter);

  $maxFilter = $('<span></span>').attr('id', 'maxFilter')
  .rateYo({rating: 5, halfStar: true, starWidth: "20px", ratedFill: color,
    onSet: function (rating, rateYoInstance) {
      if (minRate <= rating){
        maxRate = rating;
        onMapMove();
      } else {
        maxRate = rating;
        minRate = rating;
        $('#minFilter').rateYo("rating", minRate);
      }
    }
  })
  .appendTo($filterElt);
  $('<div></div>').addClass('filterLabel').text(noteMaximumText).appendTo($maxFilter);
}
