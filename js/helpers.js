// Vérifie si chaque marqueur est dans la zone et compris dans la filtre
function onMapMove(){
  $.each(allMarkers, function (index, marker){
		var restaurantElt = $('.restaurant:eq(' + index + ')');
		if(map.getBounds().contains(marker.getPosition()) && allRestaurants[index].isInRange() == true){
			marker.setVisible(true);
			restaurantElt.removeClass('hide');
		} else {
			marker.setVisible(false);
			restaurantElt.addClass('hide');
		}
		if(map.getBounds().contains(marker.getPosition()) && marker.getVisible() && marker.getVisible() && allRestaurants[index].isInRange() == true){
			restaurantElt.removeClass('hide');
		} else {
			restaurantElt.addClass('hide');
		}
	});
}

function goToRestaurant(event){
  $index = event.data.index;
  $lat = allRestaurants[$index].lat;
  $lng = allRestaurants[$index].lng;

  $pos = new google.maps.LatLng($lat,$lng);
  map.setCenter($pos);
  map.setZoom(10);
}

// Retourne un nombre aléatoire compris entre min et max
function getRandomNumber(min, max){
  return Math.floor(min + Math.random()*(max - min));
}
