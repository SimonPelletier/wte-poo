
var colorTab = [ '#6c4632','#377e00','#de4625','#0e1545','#750c0c','#006949','#395667','#8e006a' ]

var color = colorTab[getRandomNumber(0, colorTab.length)];
var colorText = '#ffffff';

// Note minimum et maximum
var minRate = 0;
var maxRate = 5;

var noteMinimumText = 'Note minimum';
var noteMaximumText = 'Note maximum';


// TABLEAU - contient tous les objets Restaurant
var allRestaurants = new Array();
// TABLEAU - contient tous les objets Marker
var allMarkers = new Array();

// Position par défaut
var defaultPosition = {
  lat: parseFloat(48.855254),
  lng: parseFloat(2.346161)
};

// Zoom par défaut
var defaultZoom = 8;

// Zoom minimum pour lancer la recherche GooglePlace
var minZoomAutoComplete = 14;

function initColor(){
  document.styleSheets[2].addRule("::-webkit-scrollbar-thumb", "background-color: " + color + ";");
  document.styleSheets[2].addRule("::-webkit-scrollbar-thumb:hover", "background-color: " + color + ";");

  document.styleSheets[2].addRule(".btn", "background-color: " + color + "!important;");
  document.styleSheets[2].addRule(".btn", "color: " + colorText + "!important;");

  document.styleSheets[2].addRule(".id", "color: " + color + "!important;");
  document.styleSheets[2].addRule(".name", "color: " + color + "!important;");

	var a = document.getElementById('logo');
	var svgDoc = a.contentDocument;
  var svgItems = svgDoc.getElementsByClassName('colorUpdate');
  for (var i = 0; i < svgItems.length; i++){
    svgItems[i].style.fill = color;
  }

}
